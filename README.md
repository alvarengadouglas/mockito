
# Unit tests with the Mockito framework

The project has the code of a back-end application with its unit tests using the Mockito framework for data mocks.



## Installation

The first step is to clone the project running the following command in your terminal:

```bash
  git clone https://alvarengadouglas@bitbucket.org/alvarengadouglas/mockito.git
```

After cloning the project, open it in an IDE of your choice and load the project and its dependencies.

## Running Tests

Unit tests are located in the folder:<br>
mockito\src\test\java


