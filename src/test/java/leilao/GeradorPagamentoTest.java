package leilao;

import br.com.alura.leilao.dao.LeilaoDao;
import br.com.alura.leilao.dao.PagamentoDao;
import br.com.alura.leilao.model.Lance;
import br.com.alura.leilao.model.Leilao;
import br.com.alura.leilao.model.Pagamento;
import br.com.alura.leilao.model.Usuario;
import br.com.alura.leilao.service.EnviadorDeEmails;
import br.com.alura.leilao.service.FinalizarLeilaoService;
import br.com.alura.leilao.service.GeradorDePagamento;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GeradorPagamentoTest {

    private GeradorDePagamento geradorDePagamento;

    @Mock
    private PagamentoDao pagamentoDao;

    @Captor
    private ArgumentCaptor<Pagamento> captor;

    @BeforeEach
    public void beforeEach(){
        MockitoAnnotations.initMocks(this);
        this.geradorDePagamento = new GeradorDePagamento(pagamentoDao);
    }

    @Test
    void deveriaCriarPagamentoParaVencedorDoLeilao(){
        List<Leilao> leiloes = leiloes();
        List<Lance> lances = leiloes.get(0).getLances();
        Lance lanceVencedor = lances.get(1);

        geradorDePagamento.gerarPagamento(lanceVencedor);

        Mockito.verify(pagamentoDao).salvar(captor.capture());

        Pagamento pagamento = captor.getValue();

        Assert.assertEquals(LocalDate.now().plusDays(1), pagamento.getVencimento());
        Assert.assertEquals(lanceVencedor.getValor(), pagamento.getValor());
        Assert.assertFalse(pagamento.getPago());
        Assert.assertEquals(lanceVencedor.getUsuario(), pagamento.getUsuario());
        Assert.assertEquals(leiloes.get(0), pagamento.getLeilao());
    }

    private List<Leilao> leiloes() {
        List<Leilao> listaLeiloes = new ArrayList<>();

        Leilao leilao = new Leilao("SmartPhone", new BigDecimal("500"), new Usuario("Fulano"));

        Lance primeiroLance = new Lance(new Usuario("Ciclano"), new BigDecimal("1000"));
        Lance segundoLance = new Lance(new Usuario("Beltrano"), new BigDecimal("1500"));

        leilao.propoe(primeiroLance);
        leilao.propoe(segundoLance);

        listaLeiloes.add(leilao);

        return listaLeiloes;
    }
}
